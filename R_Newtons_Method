# Title       : Newton's Method
# Description : R script for finding root of a function via Newton's method
# Author      : Robert Lulham
# Date        : April 14, 2021
# --------------------------------

MAXITS <- 50      # Maximum number of iterations
PRECISION <- 1e-8 # Desired precision
EPSILON <- 1e-14  # Division limit to avoid singularities
FOUND <- FALSE    # Variable used to determine whether method converges or not

x0 <- 0.2      # STARTING VALUE
SEED <- x0        # store it for later

# The function under consideration
f <- function(x) { x^2 - x - 1 }

# Its derivative
fprime <- function(x) { 2*x - 1 }

# Function to calculate each iteration
isaac <- function(x) { x - f(x)/fprime(x) }

# Initialise vector to store successive root estimations
vals <- c(x0)

# Function that actually does the work
newton <- function(){
  for(iter in 1:MAXITS){
    # Check if fprime is too small to continue, to avoid division by zero:
    if(abs(fprime(x0)) < EPSILON){
      print("Error: division by 0; possible bad seed.")
      break
    }
    
    # Calculate the next iteration:
    x1 <- isaac(x0)
    # Append its value to the vals vector:
    vals[iter+1] <- x1
    
    # If the desired precision is obtained, print the answer and break:
    if(abs(x1-x0) < PRECISION){
      FOUND <- TRUE
      print(sprintf("Converged! The root is %.8f",x1))
      break
    }
    
    # Set x0 to the new value for the next iteration:
    x0 <- x1
  }
  
  # If no root found, print this message: 
  if(FOUND==FALSE){
    print("Did not converge!")
  }
  
  # Return the vector of successive root estimations
  return(vals) 
  
}

vals <- newton()

# Print successive root estimations to the console
print("Iterations were:")
print(vals)

# ==================== PLOTTING =============================
library(RColorBrewer)
cols <- brewer.pal(9,"Paired")

par(mfrow=c(1,2)) # create 1x2 subplot
# Plot the estimations at each iteration
iters <- seq(1,length(vals),1)
plot(iters,vals,
main="Newton's Method Iterations",
ylab="Root estimation",
xlab="Iteration number",
type="o",
col="blue")

# Plot original function in second subplot
shoulder = 0.1 # Indirectly controls the plot x-limits
if(min(vals) > SEED){
  xmin <- SEED-shoulder
  xmax <- min(vals)+shoulder
} else {
  xmin <- min(vals)-shoulder
  xmax <- SEED+shoulder
}
curve(f, from = xmin, to = xmax,
      main = "Original function",
      col = 2,
      lwd = 2)
# Plot X-axis
lines(c(xmin,xmax),c(0,0), col="black")
# Plot each iteration's tangent line
formax <- length(vals)-1
for(i in 1:formax){
  lines( c(vals[i+1],vals[i]), c(0,f(vals[i])), col=cols[i+1] )
  lines( c(vals[i],vals[i]), c(0,f(vals[i])), col="black" )
}
# Label the first four estimations
text(vals[1:4],c(0,0,0,0), c("Start","x1","x2","x3"), pos=1)

